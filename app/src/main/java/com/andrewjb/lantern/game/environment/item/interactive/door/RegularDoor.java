package com.andrewjb.lantern.game.environment.item.interactive.door;

import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;

import static com.andrewjb.lantern.game.environment.item.ItemType.KEY;

public class RegularDoor extends Door {

    public RegularDoor(final String name) {
        super(name);
    }

    @Override
    public ItemResult combineWith(final Item item) {

        if (item.getItemType() == KEY){
            setOpen(true);

        }
        return null;
    }
}
