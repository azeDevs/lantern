package com.andrewjb.lantern.game.environment;

public class Room extends Landmark{

    private Wall north;
    private Wall east;
    private Wall south;
    private Wall west;

    public Wall getWall(final Direction direction){
        switch (direction){
            case EAST:
                return east;
            case SOUTH:
                return south;
            case WEST:
                return west;
            default:
                return north;
        }
    }

    public void initialize(){
        north.setRoom(this);
        east.setRoom(this);
        south.setRoom(this);
        west.setRoom(this);
    }
}
