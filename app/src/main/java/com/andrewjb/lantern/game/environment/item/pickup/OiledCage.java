package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

import static com.andrewjb.lantern.game.environment.item.ItemType.OILED_CAGE;

public class OiledCage extends InteractiveItem {

    public OiledCage() {
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return OILED_CAGE;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        switch (item.getItemType()){
            case WICK:
                return new ItemResult(new Lantern(), R.string.placeholder);
            default:
                return new ItemResult();
        }
    }
}
