package com.andrewjb.lantern.game.environment.item;

import android.support.annotation.StringRes;

public abstract class InteractiveItem extends Item {

    public InteractiveItem(@StringRes final int description) {
        super(description);
    }

    @Override
    public boolean isInteractive() {
        return true;
    }

    @Override
    public boolean canPickUp() {
        return false;
    }

}
