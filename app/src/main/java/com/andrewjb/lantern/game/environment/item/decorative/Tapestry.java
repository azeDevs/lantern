package com.andrewjb.lantern.game.environment.item.decorative;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.DecorativeItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

public class Tapestry extends DecorativeItem {

    public Tapestry(){
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return ItemType.TAPESTRY;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        return new ItemResult();
    }


}
