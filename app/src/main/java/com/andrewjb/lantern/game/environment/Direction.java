package com.andrewjb.lantern.game.environment;

public enum Direction {

    NORTH {
        @Override
        public Direction getRight() {
            return EAST;
        }

        @Override
        public Direction getLeft() {
            return WEST;
        }
    },
    EAST {
        @Override
        public Direction getRight() {
            return SOUTH;
        }

        @Override
        public Direction getLeft() {
            return NORTH;
        }
    },
    SOUTH {
        @Override
        public Direction getRight() {
            return WEST;
        }

        @Override
        public Direction getLeft() {
            return EAST;
        }
    },
    WEST {
        @Override
        public Direction getRight() {
            return NORTH;
        }

        @Override
        public Direction getLeft() {
            return SOUTH;
        }
    };

    public abstract Direction getRight();
    public abstract Direction getLeft();

}
