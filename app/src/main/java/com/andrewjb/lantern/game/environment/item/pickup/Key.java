package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractivePickup;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

public class Key extends InteractivePickup {

    public Key() {
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return ItemType.KEY;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        return new ItemResult();
    }


}
