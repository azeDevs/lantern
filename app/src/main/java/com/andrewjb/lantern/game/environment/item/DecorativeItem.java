package com.andrewjb.lantern.game.environment.item;

import android.support.annotation.StringRes;

public abstract class DecorativeItem extends Item {

    public DecorativeItem(@StringRes final int description) {
        super(description);
    }

    @Override
    public boolean isInteractive() {
        return false;
    }

    @Override
    public boolean canPickUp() {
        return false;
    }
}
