package com.andrewjb.lantern.game.environment;

import com.andrewjb.lantern.game.environment.item.interactive.door.Door;

import java.util.HashMap;

public class DoorMan {

    private static final HashMap<String, Door> doorMap = new HashMap<>();

    public HashMap<String, Door> getDoorMap() {
        return doorMap;
    }

    public void clear(){
        doorMap.clear();
    }

    public boolean hasDoorWithName(String name){
        return doorMap.containsKey(name);
    }

    public Door get(final String name){
        return doorMap.get(name);
    }

    public void put(final Door door){
        doorMap.put(door.getName(), door);
    }
}
