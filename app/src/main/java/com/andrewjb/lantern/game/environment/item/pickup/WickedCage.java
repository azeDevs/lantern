package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

public class WickedCage extends InteractiveItem {

    public WickedCage(){
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return ItemType.WICKED_CAGE;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        switch (item.getItemType()){
            case OIL:
                return new ItemResult(new Lantern(), R.string.placeholder);
            default:
                return new ItemResult();
        }
    }
}
