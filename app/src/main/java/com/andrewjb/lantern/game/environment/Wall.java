package com.andrewjb.lantern.game.environment;

import android.graphics.drawable.Drawable;

import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.interactive.door.Door;

import java.util.List;

public class Wall extends Landmark{

    private Direction direction;
    private List<Item> items;
    private Door door;
    private Room room;
    private Drawable background;

    public Direction getDirection() {
        return direction;
    }

    public void setRoom(final Room room) {
        this.room = room;

        if (door != null){
            door.registerRoom(room);
        }
    }

    public Door getDoor() {
        return door;
    }

    public List<Item> getItems() {
        return items;
    }

    public boolean removeItem(Item item) {
        return items.remove(item);
    }

    public Drawable getBackground(){
        return background;
    }
}
