package com.andrewjb.lantern.game.player;

public enum TapMode {
    MOVE, LOOK, GRAB
}
