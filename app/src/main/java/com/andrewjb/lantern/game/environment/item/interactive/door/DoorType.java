package com.andrewjb.lantern.game.environment.item.interactive.door;

public enum DoorType {

    REGULAR_DOOR {
        @Override
        public Door getDoor(final String name) {
            return new RegularDoor(name);
        }
    };

    public abstract Door getDoor(String name);
}
