package com.andrewjb.lantern.game.environment.item;

import android.support.annotation.StringRes;

import com.andrewjb.lantern.R;

public class ItemResult {

    private final Item item;
    private final boolean success;
    @StringRes private final int description;

    public ItemResult() {
        item = null;
        success = false;
        description = R.string.placeholder;
    }

    public ItemResult(final Item item, @StringRes final int description) {
        this.description = description;
        this.item = item;
        this.success = item != null;
    }

    public Item getItem() {
        return item;
    }

    public boolean wasSuccessful() {
        return success;
    }

    public int getDescription() {
        return description;
    }
}
