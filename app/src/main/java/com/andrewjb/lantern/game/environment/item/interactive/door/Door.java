package com.andrewjb.lantern.game.environment.item.interactive.door;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.Room;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.ItemType;

import java.util.ArrayList;
import java.util.List;

public abstract class Door extends InteractiveItem {

    private final String name;
    private boolean open = false;
    private final List<Room> rooms = new ArrayList<>();

    public Door(final String name) {
        super(R.string.placeholder);
        this.name = name;
    }

    public void registerRoom(Room room){
        if (!rooms.contains(room)){
            rooms.add(room);
        }
    }

    @Override
    public int getImageRes() {
        return R.drawable.prop_darkdoor;
    }

    public Room passThrough(Room currentRoom){
        for (Room room : rooms){
            if (currentRoom != room){
                return room;
            }
        }
        return null;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.DOOR;
    }

    public String getName() {
        return name;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(final boolean open) {
        this.open = open;
    }
}
