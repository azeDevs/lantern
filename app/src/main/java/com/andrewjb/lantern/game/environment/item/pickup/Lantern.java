package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

import static com.andrewjb.lantern.game.environment.item.ItemType.LANTERN;

public class Lantern extends InteractiveItem {

    public Lantern(){
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return LANTERN;
    }

    @Override
    public ItemResult combineWith(final Item item){
        return new ItemResult(null, R.string.placeholder);
    }
}
