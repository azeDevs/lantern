package com.andrewjb.lantern.game;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.andrewjb.lantern.game.environment.DoorMan;
import com.andrewjb.lantern.game.environment.Room;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.interactive.door.Door;
import com.andrewjb.lantern.gson.DoorDeserializer;
import com.andrewjb.lantern.gson.DrawableDeserializer;
import com.andrewjb.lantern.gson.ItemDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Board {

    private Room[] rooms;
    private static final DoorMan doorMan = new DoorMan();

    public Room[] getRooms() {
        return rooms;
    }

    public void initialize(){
        doorMan.clear();

        for (Room room : rooms){
            room.initialize();
        }
    }

    public static Board fromAssets(Context context, String fileName){
        try {
            InputStream stream = context.getAssets().open(fileName);

            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }

            String result = sb.toString();

            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Item.class, new ItemDeserializer());
            builder.registerTypeAdapter(Door.class, new DoorDeserializer(doorMan));
            builder.registerTypeAdapter(Drawable.class, new DrawableDeserializer(context.getResources()));
            Gson gson = builder.create();

            return gson.fromJson(result, Board.class);
        }
        catch (IOException e){
            Log.e("MainActivity", "IOException: Unable to load game");
        }

        return null;
    }
}
