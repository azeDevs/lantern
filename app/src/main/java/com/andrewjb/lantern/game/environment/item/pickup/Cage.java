package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

import static com.andrewjb.lantern.game.environment.item.ItemType.CAGE;

public class Cage extends InteractiveItem {

    public Cage() {
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return CAGE;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        switch (item.getItemType()){
            case OIL:
                return new ItemResult(new OiledCage(), R.string.placeholder);
            case WICK:
                return new ItemResult(new WickedCage(), R.string.placeholder);
            default:
                return new ItemResult();
        }
    }
}
