package com.andrewjb.lantern.game.environment.item.decorative;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.DecorativeItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

public class Table extends DecorativeItem {

    public Table(){
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return ItemType.TABLE;
    }

    @Override
    public int getImageRes() {
        return R.drawable.prop_table;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        return new ItemResult();
    }


}
