package com.andrewjb.lantern.game.environment.item;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.Entity;

public abstract class Item extends Entity {

    private float x = 0;
    private float y = 0;
    private float size = 0;

    public Item(@StringRes final int description) {
        super(description);
    }

    public abstract boolean isInteractive();
    public abstract boolean canPickUp();
    public abstract ItemType getItemType();
    public abstract ItemResult combineWith(Item item);

    public float getX() {
        return x;
    }

    public void setX(final float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(final float y) {
        this.y = y;
    }

    public float getSize() {
        return size;
    }

    public void setSize(final float size) {
        this.size = size;
    }

    public boolean isWithinBounds(final float xPercentage, final float yPercentage){
        return (xPercentage >= x && xPercentage <= x + size) && (yPercentage >= y && yPercentage <= y + size);
    }

    @DrawableRes
    public int getImageRes(){
        return R.drawable.item_default;
    }
}
