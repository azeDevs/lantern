package com.andrewjb.lantern.game.environment;

import android.support.annotation.StringRes;

import com.andrewjb.lantern.R;

public class Entity {

    private @StringRes int description;

    public Entity() {
        description = R.string.placeholder;
    }

    public Entity(@StringRes final int description) {
        this.description = description;
    }

    @StringRes
    public int getDescription(){
        return description;
    }

}
