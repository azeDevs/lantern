package com.andrewjb.lantern.game.environment.item;

import com.andrewjb.lantern.game.environment.item.decorative.Table;
import com.andrewjb.lantern.game.environment.item.decorative.Tapestry;
import com.andrewjb.lantern.game.environment.item.pickup.Cage;
import com.andrewjb.lantern.game.environment.item.pickup.Key;
import com.andrewjb.lantern.game.environment.item.pickup.Lantern;
import com.andrewjb.lantern.game.environment.item.pickup.Oil;
import com.andrewjb.lantern.game.environment.item.pickup.OiledCage;
import com.andrewjb.lantern.game.environment.item.pickup.Wick;
import com.andrewjb.lantern.game.environment.item.pickup.WickedCage;


public enum ItemType {

    WICK {
        @Override
        public Item getItem() {
            return new Wick();
        }
    },
    CAGE {
        @Override
        public Item getItem() {
            return new Cage();
        }
    },
    OIL {
        @Override
        public Item getItem() {
            return new Oil();
        }
    },
    WICKED_CAGE {
        @Override
        public Item getItem() {
            return new WickedCage();
        }
    },
    OILED_CAGE {
        @Override
        public Item getItem() {
            return new OiledCage();
        }
    },
    LANTERN {
        @Override
        public Item getItem() {
            return new Lantern();
        }
    },
    TAPESTRY {
        @Override
        public Item getItem() {
            return new Tapestry();
        }
    },
    DOOR {
        @Override
        public Item getItem() {
            return null;
        }
    },
    KEY {
        @Override
        public Item getItem() {
            return new Key();
        }
    },
    TABLE {
        @Override
        public Item getItem() {
            return new Table();
        }
    };

    public abstract Item getItem();

}
