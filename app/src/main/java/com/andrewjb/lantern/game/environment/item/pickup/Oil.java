package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

import static com.andrewjb.lantern.game.environment.item.ItemType.OIL;

public class Oil extends InteractiveItem {
    
    public Oil(){
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return OIL;
    }

    @Override
    public int getImageRes() {
        return R.drawable.prop_cage;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        switch (item.getItemType()){
            case WICKED_CAGE:
                return new ItemResult(new Lantern(), R.string.placeholder);
            case CAGE:
                return new ItemResult(new OiledCage(), R.string.placeholder);
            default:
                return new ItemResult();
        }
    }
}
