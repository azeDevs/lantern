package com.andrewjb.lantern.game.player;

import com.andrewjb.lantern.game.environment.Direction;

public class Player {

    public interface OnDirectionChangeListener{
        void onDirectionChange(Direction direction);
    }

    private final String name;

    private final Inventory inventory = new Inventory();

    private Direction direction = Direction.NORTH;
    public TapMode tapMode;

    private OnDirectionChangeListener onDirectionChangeListener;

    public Player(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setOnDirectionChangeListener(final OnDirectionChangeListener onDirectionChangeListener) {
        this.onDirectionChangeListener = onDirectionChangeListener;
    }

    private void notifyDirectionChange(){
        if (onDirectionChangeListener != null){
            onDirectionChangeListener.onDirectionChange(direction);
        }
    }

    public Direction turnRight(){
        setDirection(direction.getRight());
        return direction;
    }

    public Direction turnLeft(){
        setDirection(direction.getLeft());
        return direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(final Direction direction) {
        this.direction = direction;
        notifyDirectionChange();
    }

    public TapMode getTapMode() {
        return tapMode;
    }

    public void setTapMode(final TapMode tapMode) {
        this.tapMode = tapMode;
    }
}
