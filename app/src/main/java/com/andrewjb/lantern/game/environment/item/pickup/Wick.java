package com.andrewjb.lantern.game.environment.item.pickup;

import com.andrewjb.lantern.R;
import com.andrewjb.lantern.game.environment.item.InteractiveItem;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemResult;
import com.andrewjb.lantern.game.environment.item.ItemType;

import static com.andrewjb.lantern.game.environment.item.ItemType.WICK;

public class Wick extends InteractiveItem {

    public Wick() {
        super(R.string.placeholder);
    }

    @Override
    public ItemType getItemType() {
        return WICK;
    }

    @Override
    public ItemResult combineWith(final Item item) {
        switch (item.getItemType()){
            case CAGE:
                return new ItemResult(new WickedCage(), R.string.placeholder);
            case OILED_CAGE:
                return new ItemResult(new Lantern(), R.string.placeholder);
            default:
                return new ItemResult();
        }
    }
}
