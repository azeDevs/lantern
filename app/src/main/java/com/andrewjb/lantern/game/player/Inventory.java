package com.andrewjb.lantern.game.player;

import com.andrewjb.lantern.game.environment.item.InteractivePickup;
import com.andrewjb.lantern.game.environment.item.Item;

import java.util.ArrayList;

public class Inventory extends ArrayList<Item> implements InteractivePickup.OnDestroyListener{

    public static final int INVENTORY_SIZE = 5;

    public Inventory() {
        super(INVENTORY_SIZE);
    }

    @Override
    public boolean add(final Item object) {

        if (size() == INVENTORY_SIZE){
            return false;
        }

        return super.add(object);
    }

    @Override
    public void onDestroy(final InteractivePickup item) {
        if (contains(item)){
            remove(item);
        }
    }
}
