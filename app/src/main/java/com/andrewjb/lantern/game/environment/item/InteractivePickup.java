package com.andrewjb.lantern.game.environment.item;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.andrewjb.lantern.R;

public abstract class InteractivePickup extends InteractiveItem {

    public interface OnDestroyListener{
        void onDestroy(InteractivePickup item);
    }

    private OnDestroyListener onDestroyListener;

    public InteractivePickup(@StringRes final int description) {
        super(description);
    }

    @Override
    public boolean canPickUp() {
        return true;
    }

    public void setOnDestroyListener(final OnDestroyListener onDestroyListener) {
        this.onDestroyListener = onDestroyListener;
    }

    public void destroy(){
        if (onDestroyListener != null){
            onDestroyListener.onDestroy(this);
        }
    }

    @DrawableRes
    public int getIconRes(){
        return R.drawable.gfx_slot;
    }
}
