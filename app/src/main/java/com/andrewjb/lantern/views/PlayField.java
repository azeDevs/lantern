package com.andrewjb.lantern.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.andrewjb.lantern.game.environment.Wall;
import com.andrewjb.lantern.game.environment.item.Item;

import java.util.List;

public class PlayField extends View implements GestureDetector.OnGestureListener{

    public interface OnItemTappedListener{
        void onItemTapped(Item item);
    }

    private Wall wall;
    private OnItemTappedListener onItemTappedListener;
    private GestureDetectorCompat gestureDetector;

    public PlayField(Context context) {
        this(context, null);
    }

    public PlayField(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        gestureDetector = new GestureDetectorCompat(context, this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY) {
            setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(widthMeasureSpec));
        }
    }

    public void setWall(final Wall wall){
        this.wall = wall;
        setBackground(wall.getBackground());
        invalidate();
    }

    public void setOnItemTappedListener(final OnItemTappedListener onItemTappedListener) {
        this.onItemTappedListener = onItemTappedListener;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        if (wall != null){
            final List<Item> items = wall.getItems();

            for (Item item : items){
                Drawable drawable = getResources().getDrawable(item.getImageRes());
                final int left = (int)(item.getX() * getWidth());
                final int top = (int)(item.getY() * getHeight());
                final int size = (int)(item.getSize() * getWidth());
                drawable.setBounds(left, top, left + size, top + size);
                drawable.draw(canvas);
            }
        }
//
//        final Paint themeDepth = new Paint();
//        themeDepth.setStyle(Paint.Style.FILL);
//
//        final float CORNER_RADIUS = 0.37f;
//        final float halfWidth = getWidth()/2;
//        final float halfHeight = getHeight()/2;
//        final float radius = (halfHeight <= halfWidth ? halfHeight : halfWidth);
//        final RectF rectDepth =  new RectF(0, 10, getWidth(), getHeight());
//
//        canvas.drawRoundRect(rectDepth, (radius * CORNER_RADIUS), (radius * CORNER_RADIUS), themeDepth);
    }

    @Override
    public boolean onDown(final MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(final MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(final MotionEvent e) {
        final float xp = e.getX() / getWidth();
        final float yp = e.getY() / getHeight();

        final List<Item> items = wall.getItems();
        Item tappedItem = null;
        for (Item item : items){
            if (item.isWithinBounds(xp, yp)){
                tappedItem = item;
            }
        }

        if (tappedItem != null && onItemTappedListener != null){
            onItemTappedListener.onItemTapped(tappedItem);
        }

        return false;
    }

    @Override
    public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX, final float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(final MotionEvent e) {

    }

    @Override
    public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX, final float velocityY) {
        return false;
    }
}
