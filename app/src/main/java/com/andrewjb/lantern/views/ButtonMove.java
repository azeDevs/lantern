package com.andrewjb.lantern.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class ButtonMove extends View {

    public ButtonMove(final Context context) {
        super(context);
    }

    public ButtonMove(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ButtonMove(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        final Paint themeDepth = new Paint();
        themeDepth.setStyle(Paint.Style.FILL);

        final float CORNER_RADIUS = 0.37f;
        final float halfWidth = getWidth()/2;
        final float halfHeight = getHeight()/2;
        final float radius = (halfHeight <= halfWidth ? halfHeight : halfWidth);
        final RectF rectFace =   new RectF(0, 0, getWidth(), getHeight()-8);
        final RectF rectDepth =  new RectF(0, 10, getWidth(), getHeight());

        canvas.drawRoundRect(rectDepth, (radius * CORNER_RADIUS), (radius * CORNER_RADIUS), themeDepth);
    }
}

