package com.andrewjb.lantern;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.andrewjb.lantern.game.Board;
import com.andrewjb.lantern.game.environment.Direction;
import com.andrewjb.lantern.game.environment.Room;
import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.player.Player;
import com.andrewjb.lantern.game.player.TapMode;
import com.andrewjb.lantern.views.PlayField;

public class MainActivity extends AppCompatActivity implements Player.OnDirectionChangeListener{

    private Player player;
    private Room currentRoom;
    private View btnMove, btnLook, btnGrab;
    private View moveRight, moveLeft;
    private PlayField playfield;
    private Board gameBoard;
    private Direction currentDirection;

    private View inventory1;
    private View lantern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);
        playfield = (PlayField)findViewById(R.id.playfield);

        loadGame();
        initButtons();

    }

    @Override
    public void onDirectionChange(final Direction direction) {
        playfield.setWall(currentRoom.getWall(direction));
    }

    private void loadGame() {
        player = new Player("player");
        player.setOnDirectionChangeListener(this);
        player.tapMode = TapMode.LOOK;
        currentDirection = Direction.NORTH;

        gameBoard = Board.fromAssets(this, "test.json");

        gameBoard.initialize();

        playfield.setOnItemTappedListener(new PlayField.OnItemTappedListener() {
            @Override
            public void onItemTapped(final Item item) {
                Toast.makeText(MainActivity.this,
                        item.getItemType().name() + " was tapped",
                        Toast.LENGTH_SHORT).show();
            }
        });

        renderEnvironment();
    }

    private void renderEnvironment() {
        final Room room = gameBoard.getRooms()[0];
        currentRoom = room;
        playfield.setWall(room.getWall(Direction.NORTH));
    }


    private void initButtons() {
        moveRight = findViewById(R.id.move_right);
        moveLeft = findViewById(R.id.move_left);

        moveRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                player.turnRight();
                //renderEnvironment();
            }
        });

        moveLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                player.turnLeft();
                //renderEnvironment();
            }
        });

        btnMove = findViewById(R.id.move);
        btnLook = findViewById(R.id.look);
        btnGrab = findViewById(R.id.grab);
        resetButtonHighlight();

        btnLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                player.tapMode = TapMode.LOOK;
                resetButtonHighlight();
            }
        });

        btnMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                player.tapMode = TapMode.MOVE;
                resetButtonHighlight();
            }
        });

        btnGrab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                player.tapMode = TapMode.GRAB;
                resetButtonHighlight();
            }
        });

        lantern = findViewById(R.id.lantern);
        inventory1 = findViewById(R.id.slot1);

        /*playfield.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (currentDirection == Direction.NORTH && player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "It looks like your only exit is a dark corridor.", Toast.LENGTH_LONG).show();
                else if (currentDirection == Direction.EAST && player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "The walls are cold, sunlight hasn't touched them in years.", Toast.LENGTH_LONG).show();
                else if (currentDirection == Direction.SOUTH && player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "The way you came in, it won't budge.", Toast.LENGTH_LONG).show();
                else if (currentDirection == Direction.WEST && player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "Some pictures hang on the wall, lit by a single candle.", Toast.LENGTH_LONG).show();
                renderEnvironment();
            }
        });*/

        lantern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "You hold a Lantern in your hand.", Toast.LENGTH_LONG).show();
            }
        });

        inventory1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (player.tapMode == TapMode.LOOK)
                    Toast.makeText(MainActivity.this, "It's a Gold Key.", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void resetButtonHighlight() {
        btnLook.setBackground(getResources().getDrawable(R.drawable.btn_look));
        btnMove.setBackground(getResources().getDrawable(R.drawable.btn_move));
        btnGrab.setBackground(getResources().getDrawable(R.drawable.btn_grab));
        if (player.tapMode == TapMode.LOOK) {
            btnLook.setBackground(getResources().getDrawable(R.drawable.btn_look_on));
            moveLeft.setVisibility(View.INVISIBLE);
            moveRight.setVisibility(View.INVISIBLE);
        }
        if (player.tapMode == TapMode.MOVE) {
            btnMove.setBackground(getResources().getDrawable(R.drawable.btn_move_on));
            moveLeft.setVisibility(View.VISIBLE);
            moveRight.setVisibility(View.VISIBLE);
        }
        if (player.tapMode == TapMode.GRAB) {
            btnGrab.setBackground(getResources().getDrawable(R.drawable.btn_grab_on));
            moveLeft.setVisibility(View.INVISIBLE);
            moveRight.setVisibility(View.INVISIBLE);
        }
    }

}
