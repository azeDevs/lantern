package com.andrewjb.lantern.gson;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class DrawableDeserializer implements JsonDeserializer<Drawable>{

    private final Resources resources;

    public DrawableDeserializer(final Resources resources){
        this.resources = resources;
    }

    @Override
    public Drawable deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        String name = json.getAsString();

        final int id  = resources.getIdentifier(name, "drawable", "com.andrewjb.lantern");

        return resources.getDrawable(id);
    }
}
