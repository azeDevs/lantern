package com.andrewjb.lantern.gson;

import com.andrewjb.lantern.game.environment.DoorMan;
import com.andrewjb.lantern.game.environment.item.interactive.door.Door;
import com.andrewjb.lantern.game.environment.item.interactive.door.DoorType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class DoorDeserializer implements JsonDeserializer<Door> {

    private final DoorMan doorMan;

    public DoorDeserializer(final DoorMan doorMan){
        this.doorMan = doorMan;
    }

    @Override
    public Door deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {

        Door door = null;
        JsonObject object = json.getAsJsonObject();
        final String doorName = object.get("name").getAsString();
        if (doorMan.hasDoorWithName(doorName)){
            return doorMan.get(doorName);
        }
        else{
            final DoorType doorType = DoorType.valueOf(object.get("type").getAsString());
            door = doorType.getDoor(doorName);
            doorMan.put(door);
            return door;
        }
    }
}
