package com.andrewjb.lantern.gson;

import com.andrewjb.lantern.game.environment.item.Item;
import com.andrewjb.lantern.game.environment.item.ItemType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ItemDeserializer implements JsonDeserializer<Item> {

    @Override
    public Item deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        ItemType itemType = ItemType.valueOf(object.get("itemType").getAsString());
        Item item = itemType.getItem();

        item.setX(object.get("x").getAsFloat());
        item.setY(object.get("y").getAsFloat());
        item.setSize(object.get("size").getAsFloat());

        return item;
    }
}
